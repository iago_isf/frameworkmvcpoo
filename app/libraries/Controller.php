<?php
class Controller
{

  public function __construct()
  {

  }
  
  public function model ($modelo) 
  {
    require_once '../app/models/' . $modelo . '.php';

    $nuevoModelo = new $modelo;
    return $nuevoModelo;
  }

  public function view ($vista, $datos=[]) 
  {
    if (file_exists('../app/views/' . $vista . '.php')) { 
      require_once '../app/views/' . $vista . '.php';
    } else {
      die('No se ha encontrado la vista');
    }
  }
}
?>